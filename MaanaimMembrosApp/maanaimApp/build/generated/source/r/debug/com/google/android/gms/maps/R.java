/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.google.android.gms.maps;

public final class R {
	public static final class attr {
		public static final int ambientEnabled = 0x7f0100b5;
		public static final int buttonSize = 0x7f0100ca;
		public static final int cameraBearing = 0x7f0100a6;
		public static final int cameraTargetLat = 0x7f0100a7;
		public static final int cameraTargetLng = 0x7f0100a8;
		public static final int cameraTilt = 0x7f0100a9;
		public static final int cameraZoom = 0x7f0100aa;
		public static final int circleCrop = 0x7f0100a4;
		public static final int colorScheme = 0x7f0100cb;
		public static final int imageAspectRatio = 0x7f0100a3;
		public static final int imageAspectRatioAdjust = 0x7f0100a2;
		public static final int liteMode = 0x7f0100ab;
		public static final int mapType = 0x7f0100a5;
		public static final int scopeUris = 0x7f0100cc;
		public static final int uiCompass = 0x7f0100ac;
		public static final int uiMapToolbar = 0x7f0100b4;
		public static final int uiRotateGestures = 0x7f0100ad;
		public static final int uiScrollGestures = 0x7f0100ae;
		public static final int uiTiltGestures = 0x7f0100af;
		public static final int uiZoomControls = 0x7f0100b0;
		public static final int uiZoomGestures = 0x7f0100b1;
		public static final int useViewLifecycle = 0x7f0100b2;
		public static final int zOrderOnTop = 0x7f0100b3;
	}
	public static final class color {
		public static final int common_action_bar_splitter = 0x7f0c00a0;
		public static final int common_google_signin_btn_text_dark = 0x7f0c0108;
		public static final int common_google_signin_btn_text_dark_default = 0x7f0c00a1;
		public static final int common_google_signin_btn_text_dark_disabled = 0x7f0c00a2;
		public static final int common_google_signin_btn_text_dark_focused = 0x7f0c00a3;
		public static final int common_google_signin_btn_text_dark_pressed = 0x7f0c00a4;
		public static final int common_google_signin_btn_text_light = 0x7f0c0109;
		public static final int common_google_signin_btn_text_light_default = 0x7f0c00a5;
		public static final int common_google_signin_btn_text_light_disabled = 0x7f0c00a6;
		public static final int common_google_signin_btn_text_light_focused = 0x7f0c00a7;
		public static final int common_google_signin_btn_text_light_pressed = 0x7f0c00a8;
		public static final int common_plus_signin_btn_text_dark = 0x7f0c010a;
		public static final int common_plus_signin_btn_text_dark_default = 0x7f0c00a9;
		public static final int common_plus_signin_btn_text_dark_disabled = 0x7f0c00aa;
		public static final int common_plus_signin_btn_text_dark_focused = 0x7f0c00ab;
		public static final int common_plus_signin_btn_text_dark_pressed = 0x7f0c00ac;
		public static final int common_plus_signin_btn_text_light = 0x7f0c010b;
		public static final int common_plus_signin_btn_text_light_default = 0x7f0c00ad;
		public static final int common_plus_signin_btn_text_light_disabled = 0x7f0c00ae;
		public static final int common_plus_signin_btn_text_light_focused = 0x7f0c00af;
		public static final int common_plus_signin_btn_text_light_pressed = 0x7f0c00b0;
	}
	public static final class drawable {
		public static final int common_full_open_on_phone = 0x7f02007e;
		public static final int common_google_signin_btn_icon_dark = 0x7f02007f;
		public static final int common_google_signin_btn_icon_dark_disabled = 0x7f020080;
		public static final int common_google_signin_btn_icon_dark_focused = 0x7f020081;
		public static final int common_google_signin_btn_icon_dark_normal = 0x7f020082;
		public static final int common_google_signin_btn_icon_dark_pressed = 0x7f020083;
		public static final int common_google_signin_btn_icon_light = 0x7f020084;
		public static final int common_google_signin_btn_icon_light_disabled = 0x7f020085;
		public static final int common_google_signin_btn_icon_light_focused = 0x7f020086;
		public static final int common_google_signin_btn_icon_light_normal = 0x7f020087;
		public static final int common_google_signin_btn_icon_light_pressed = 0x7f020088;
		public static final int common_google_signin_btn_text_dark = 0x7f020089;
		public static final int common_google_signin_btn_text_dark_disabled = 0x7f02008a;
		public static final int common_google_signin_btn_text_dark_focused = 0x7f02008b;
		public static final int common_google_signin_btn_text_dark_normal = 0x7f02008c;
		public static final int common_google_signin_btn_text_dark_pressed = 0x7f02008d;
		public static final int common_google_signin_btn_text_light = 0x7f02008e;
		public static final int common_google_signin_btn_text_light_disabled = 0x7f02008f;
		public static final int common_google_signin_btn_text_light_focused = 0x7f020090;
		public static final int common_google_signin_btn_text_light_normal = 0x7f020091;
		public static final int common_google_signin_btn_text_light_pressed = 0x7f020092;
		public static final int common_ic_googleplayservices = 0x7f020093;
		public static final int common_plus_signin_btn_icon_dark = 0x7f020094;
		public static final int common_plus_signin_btn_icon_dark_disabled = 0x7f020095;
		public static final int common_plus_signin_btn_icon_dark_focused = 0x7f020096;
		public static final int common_plus_signin_btn_icon_dark_normal = 0x7f020097;
		public static final int common_plus_signin_btn_icon_dark_pressed = 0x7f020098;
		public static final int common_plus_signin_btn_icon_light = 0x7f020099;
		public static final int common_plus_signin_btn_icon_light_disabled = 0x7f02009a;
		public static final int common_plus_signin_btn_icon_light_focused = 0x7f02009b;
		public static final int common_plus_signin_btn_icon_light_normal = 0x7f02009c;
		public static final int common_plus_signin_btn_icon_light_pressed = 0x7f02009d;
		public static final int common_plus_signin_btn_text_dark = 0x7f02009e;
		public static final int common_plus_signin_btn_text_dark_disabled = 0x7f02009f;
		public static final int common_plus_signin_btn_text_dark_focused = 0x7f0200a0;
		public static final int common_plus_signin_btn_text_dark_normal = 0x7f0200a1;
		public static final int common_plus_signin_btn_text_dark_pressed = 0x7f0200a2;
		public static final int common_plus_signin_btn_text_light = 0x7f0200a3;
		public static final int common_plus_signin_btn_text_light_disabled = 0x7f0200a4;
		public static final int common_plus_signin_btn_text_light_focused = 0x7f0200a5;
		public static final int common_plus_signin_btn_text_light_normal = 0x7f0200a6;
		public static final int common_plus_signin_btn_text_light_pressed = 0x7f0200a7;
	}
	public static final class id {
		public static final int adjust_height = 0x7f0d001e;
		public static final int adjust_width = 0x7f0d001f;
		public static final int auto = 0x7f0d002b;
		public static final int dark = 0x7f0d002c;
		public static final int hybrid = 0x7f0d0020;
		public static final int icon_only = 0x7f0d0028;
		public static final int light = 0x7f0d002d;
		public static final int none = 0x7f0d0010;
		public static final int normal = 0x7f0d000c;
		public static final int satellite = 0x7f0d0021;
		public static final int standard = 0x7f0d0029;
		public static final int terrain = 0x7f0d0022;
		public static final int wide = 0x7f0d002a;
		public static final int wrap_content = 0x7f0d0015;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f0b0004;
	}
	public static final class raw {
	}
	public static final class string {
		public static final int accept = 0x7f070057;
		public static final int auth_google_play_services_client_facebook_display_name = 0x7f07007a;
		public static final int auth_google_play_services_client_google_display_name = 0x7f07007b;
		public static final int common_android_wear_notification_needs_update_text = 0x7f070013;
		public static final int common_android_wear_update_text = 0x7f070014;
		public static final int common_android_wear_update_title = 0x7f070015;
		public static final int common_google_play_services_api_unavailable_text = 0x7f070016;
		public static final int common_google_play_services_enable_button = 0x7f070017;
		public static final int common_google_play_services_enable_text = 0x7f070018;
		public static final int common_google_play_services_enable_title = 0x7f070019;
		public static final int common_google_play_services_error_notification_requested_by_msg = 0x7f07001a;
		public static final int common_google_play_services_install_button = 0x7f07001b;
		public static final int common_google_play_services_install_text_phone = 0x7f07001c;
		public static final int common_google_play_services_install_text_tablet = 0x7f07001d;
		public static final int common_google_play_services_install_title = 0x7f07001e;
		public static final int common_google_play_services_invalid_account_text = 0x7f07001f;
		public static final int common_google_play_services_invalid_account_title = 0x7f070020;
		public static final int common_google_play_services_needs_enabling_title = 0x7f070021;
		public static final int common_google_play_services_network_error_text = 0x7f070022;
		public static final int common_google_play_services_network_error_title = 0x7f070023;
		public static final int common_google_play_services_notification_needs_update_title = 0x7f070024;
		public static final int common_google_play_services_notification_ticker = 0x7f070025;
		public static final int common_google_play_services_sign_in_failed_text = 0x7f070026;
		public static final int common_google_play_services_sign_in_failed_title = 0x7f070027;
		public static final int common_google_play_services_unknown_issue = 0x7f070028;
		public static final int common_google_play_services_unsupported_text = 0x7f070029;
		public static final int common_google_play_services_unsupported_title = 0x7f07002a;
		public static final int common_google_play_services_update_button = 0x7f07002b;
		public static final int common_google_play_services_update_text = 0x7f07002c;
		public static final int common_google_play_services_update_title = 0x7f07002d;
		public static final int common_google_play_services_updating_text = 0x7f07002e;
		public static final int common_google_play_services_updating_title = 0x7f07002f;
		public static final int common_open_on_phone = 0x7f070030;
		public static final int common_signin_button_text = 0x7f070031;
		public static final int common_signin_button_text_long = 0x7f070032;
	}
	public static final class style {
	}
	public static final class styleable {
		public static final int[] LoadingImageView = { 0x7f0100a2, 0x7f0100a3, 0x7f0100a4 };
		public static final int LoadingImageView_circleCrop = 2;
		public static final int LoadingImageView_imageAspectRatio = 1;
		public static final int LoadingImageView_imageAspectRatioAdjust = 0;
		public static final int[] MapAttrs = { 0x7f0100a5, 0x7f0100a6, 0x7f0100a7, 0x7f0100a8, 0x7f0100a9, 0x7f0100aa, 0x7f0100ab, 0x7f0100ac, 0x7f0100ad, 0x7f0100ae, 0x7f0100af, 0x7f0100b0, 0x7f0100b1, 0x7f0100b2, 0x7f0100b3, 0x7f0100b4, 0x7f0100b5 };
		public static final int MapAttrs_ambientEnabled = 16;
		public static final int MapAttrs_cameraBearing = 1;
		public static final int MapAttrs_cameraTargetLat = 2;
		public static final int MapAttrs_cameraTargetLng = 3;
		public static final int MapAttrs_cameraTilt = 4;
		public static final int MapAttrs_cameraZoom = 5;
		public static final int MapAttrs_liteMode = 6;
		public static final int MapAttrs_mapType = 0;
		public static final int MapAttrs_uiCompass = 7;
		public static final int MapAttrs_uiMapToolbar = 15;
		public static final int MapAttrs_uiRotateGestures = 8;
		public static final int MapAttrs_uiScrollGestures = 9;
		public static final int MapAttrs_uiTiltGestures = 10;
		public static final int MapAttrs_uiZoomControls = 11;
		public static final int MapAttrs_uiZoomGestures = 12;
		public static final int MapAttrs_useViewLifecycle = 13;
		public static final int MapAttrs_zOrderOnTop = 14;
		public static final int[] SignInButton = { 0x7f0100ca, 0x7f0100cb, 0x7f0100cc };
		public static final int SignInButton_buttonSize = 0;
		public static final int SignInButton_colorScheme = 1;
		public static final int SignInButton_scopeUris = 2;
	}
}
